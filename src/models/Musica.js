const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const MusicaSchema = new mongoose.Schema({
    titulo: {
        type: String,
        required: true
    },
    banda: {
        type: String,
        required: true
    },
    score: {
        type: Number,
        required: true,
        default: 0
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

MusicaSchema.plugin(mongoosePaginate)
mongoose.model('Musica', MusicaSchema)