const mongoose = require('mongoose')
const Musica = mongoose.model('Musica')

module.exports = {
    async index(req, res) {
        const { page = 1 } = req.query;
        const registros = await Musica.paginate({}, { page, limit: 10 });

        return res.json(registros)
    },
    async update(req, res) {
        const registro = await Musica.findByIdAndUpdate(req.params.id, req.body, { new: true });

        // Avisa o front-end
        req.io.emit('editMusica', registro);

        return res.json(registro)
    },
    async store(req, res) {
        const registro = await Musica.create(req.body)

        return res.json(registro)
    }
}