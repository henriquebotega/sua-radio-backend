const express = require('express')
const routes = express.Router()

const MusicasController = require('./controllers/MusicasController')
routes.get('/musicas', MusicasController.index);
routes.put('/musicas/:id', MusicasController.update);
routes.post('/musicas', MusicasController.store);

module.exports = routes